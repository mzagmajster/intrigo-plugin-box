<?php
/*
  Plugin Name: {{ plugin_name }}
  Plugin URI: {{ plugin_uri }}
  Description: {{ plugin_description }}
  Version: {{ plugin_version }}
  Author: {{ plugin_author }}
  Author URI: {{ plugin_author_uri }}
  License: GPLv3
  License URI: https://choosealicense.com/licenses/gpl-3.0/
 */

{% include "inc-notice.txt" %}

require_once 'vendor/autoload.php';

require_once __DIR__ . '/RefineIt/Support/boot.php';


return [
	/**
	 * Initialize core elements.
	 *
	 * Note: All elements under this section are required for plugin to function - do not change anything unless you 
	 * really know what you are doing.
	 * 
	 */
	
	new \RefineIt\Support\Autoloader('RefineIt'),

	/**
	 * Load modules.
	 *
	 * Construct object for each module you are planning to use and place it under this section.
	 *
	 * Note: For each module you have to Autoloader´s include list to enable construction of module object.
	 *
	 * To fire up new module add lines below and change name of the module to correct one:
	 *
	 * new \RefineIt\Support\Autoloader('Example'),
	 * new \Example\PluginShell(),
	 * 
	 */
	{{ load_modules | raw }}

	/**
	 * Initialize last components.
	 *
	 * Note: All elements under this section are required for plugin to function - do not change anything unless you 
	 * really know what you are doing.
	 * 
	 */
	
	register_activation_hook(__FILE__, 'RefineIt\Support\activation'),
	register_deactivation_hook(__FILE__, 'RefineIt\Support\deactivation')
];

