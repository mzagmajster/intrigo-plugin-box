<?php

{% include "inc-notice.txt" %}

declare(strict_types=1);

namespace {{ namespace_name }};

use RefineIt\Support\Plugin\ModuleBase;
use {{ namespace_name }}\Info;

/**
 * PluginShell class.
 *
 * Please refer to:
 * http://maticzagmajster.ddns.net/docs/intrigo-plugin-box/ 
 * for more details.
 */
class PluginShell extends ModuleBase {
	public function __construct() {
		parent::__construct();
	}

	public function g(): array {
		return Info::g();
	}

	public function activation(): void {
	
	}

	public function deactivation(): void {
	
	}

	public function register_scripts(): void {
	
	}

	public function register_styles(): void {

	}

	public function run_plugin(): void {
		
	}

}
