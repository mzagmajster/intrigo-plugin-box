<?php

{% include "inc-notice.txt" %}
declare(strict_types=1);

/**
 * --------------------------------------------------------------------------------
 * THIS FILE SHOULD NOT BE EDITED MANUALLY!
 * 
 * Please refer to: 
 * http://maticzagmajster.ddns.net/docs/intrigo-plugin-box/ 
 * for more details.
 * --------------------------------------------------------------------------------
 */

namespace {{ namespace_name }};

use RefineIt\Support\Plugin\InfoInterface;


class Info implements InfoInterface {

	/**
	 * Config of the scope (module) the class is in.
	 *
	 * When config is loaded this is flat associative array holding all the values 
	 * from files it can find in relevant config folder.
	 * 
	 * @var mixed
	 */
	protected static $config = NULL;

	/**
	 * Configuration files we are looking for in every config folder.
	 * 
	 * @var array
	 */
	const VALID_CONFIG_FILES = [
		'option.php',
		'context.php',
		'structure.php'
	];

	/**
	 * Name of the config folder in given scope.
	 *
	 * @var string
	 */
	const CONFIG_FOLDER = 'config';

	/**
	 * Gets root path of the module.
	 * 
	 * @return string Absolute path.
	 */
	public static function root_path(): string {
		return \trailingslashit(__DIR__);
	}

	/**
	 * Gets root url of the module.
	 * 
	 * @return string Absolute path.
	 */
	public static function root_url(): string {
		return \plugin_dir_url(__FILE__);
	}

	/**
	 * Gets root plugin path.
	 * 
	 * @return string Absolute path.
	 */
	public static function root_plugin_path(): string {
		return \trailingslashit(\realpath((self::root_path() . '/..')));
	}

	/**
	 * Offers access to $config variable.
	 * 
	 * @return array
	 */
	public static function g(): array {

		if (self::$config === NULL) {
			$config_path = self::root_path() . '/' . self::CONFIG_FOLDER;
			$config_reader_factory = new \Helhum\ConfigLoader\ConfigurationReaderFactory($config_path);

			$config_readers = [];
			foreach (self::VALID_CONFIG_FILES as $f) {
				$path = $config_path . '/' . $f;
				if (!file_exists($path)) {
					continue;
				}

				$config_readers[] = $config_reader_factory->createReader($path);
			}

			$config_loader = new \Helhum\ConfigLoader\ConfigurationLoader($config_readers);
			self::$config = $config_loader->load();
		}

		return self::$config;
	}

	/**
	 * Gets configuration just in one file.
	 * 
	 * @param  string $filename Filename from config folder.
	 * @return array
	 */
	public static function get(string $filename): array {
		$config_path = self::root_path() . '/' . self::CONFIG_FOLDER;
		$config_reader_factory = new \Helhum\ConfigLoader\ConfigurationReaderFactory($config_path);

		$path = $config_path . '/' . $filename;

		if (!file_exists($path)) {
			return [];
		}

		$config_loader = new \Helhum\ConfigLoader\ConfigurationLoader([
			$config_reader_factory->createReader($path)
		]);
		return $config_loader->load();
	}

}
