<?php

declare(strict_types=1);
require_once 'vendor/autoload.php';


/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
	use Robo\Task\Base\loadShortcuts;

	/**
	 * Relative path to config folder of source module.
	 *
	 * @var string
	 */
	const REFINE_IT_CONFIG_PATH = './RefineIt/config/';

	/**
	 * Relative path to code templates.
	 *
	 * @var string
	 */
	const CODE_FOLDER_PATH = '../code/';

	/**
	 * Relative path to root directory for all modules.
	 *
	 * @var string
	 */
	const MODULE_BASE_PATH = './';

	/**
	 * Relative path to source module.
	 *
	 * @var string
	 */
	const SOURCE_MODULE_PATH = './RefineIt/';

	/**
	 * Relative path to ipb-info.json file - contains basic information to enable use of code templates.
	 *
	 * @var string
	 */
	const IPB_INFO_FILE = './.ipb-info.json';

	/**
	 * Holder of configuration required for this class to function.
	 * 
	 * @var array
	 */
	private $config;

	/**
	 * Code templates loader.
	 * 
	 * @var \Twig\Loader\FilesystemLoader
	 */
	private $loader;

	/**
	 * Code templates environments,
	 * 
	 * @var \Twig\Environment
	 */
	private $env;

	/**
	 * Constructor.
	 */
	function __construct() {
		// For this class to be useful we only really need structure.php and context.php
		$this->config = [
			'structure' => include(__DIR__ . '/' . self::REFINE_IT_CONFIG_PATH . 'structure' . '.php'),
			'context' => include(__DIR__ . '/' . self::REFINE_IT_CONFIG_PATH . 'context' . '.php')
		];

		$this->loader = new \Twig\Loader\FilesystemLoader(realpath(self::CODE_FOLDER_PATH));
		$this->env = new \Twig\Environment($this->loader);
	}

	/**
	 * Duplicates source module in destination.
	 * 
	 * @param  string $source      Absolute path to root of module.
	 * @param  string $destination Absolute path to root of destination where we want new module to be.
	 * @return void
	 */
	private function copyDir(string $source, string $destination): void {
		$dir = opendir($source);
		@mkdir($destination);
		while(false !== ( $file = readdir($dir)) ) {
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($source . '/' . $file)) {
					$this->copyDir($source . '/' . $file,  $destination . '/' . $file);
				}
				else if (!is_dir($source . '/' . $file)) {
					copy($source . '/' . $file, $destination . '/' . $file);
				}
			}

		}
		closedir($dir);
	}

	/**
	 * Checks file existance before generating new file.
	 * 
	 * @param  string       $template    Filename from CODE_FOLDER_PATH directory.
	 * @param  array        $data        Values to use in template.
	 * @param  string       $destination Absolute path to new file.
	 * @param  bool|boolean $override    If set tu true method will overwrite file in case it exists. Default: false
	 * @return string                    Returns one of the following strings: [FILE_ALREADY_EXISTS, FILE_CREATED] accordingly.
	 */
	private function generateFile(string $template, array $data, string $destination, bool $override=false): string {
		if (file_exists($destination) && !$override) {
			$this->say("It appears that file: " . $destination . " already exists. Exiting.");
			return 'FILE_ALREADY_EXISTS';
		}

		file_put_contents(
			$destination,
			$this->env->render(
				$template,
				$data
		));

		return 'FILE_CREATED';
	}

	/**
	 * Generates plugin main filename based on plugin name.
	 * 
	 * @param  string $s Plugin name.
	 * @return string    Name of the file or string 'n-a' if value could not be slugified.
	 */
	private function slugify(string $s): string {
		$s = preg_replace('~[^\pL\d]+~u', '-', $s);
		$s = iconv('utf-8', 'us-ascii//TRANSLIT', $s);
		$s = preg_replace('~[^-\w]+~', '', $s);
		$s = trim($s, '-');
		$s = preg_replace('~-+~', '-', $s);
		$s = strtolower($s);

		if (empty($s)) {
			return 'n-a';
		}

		return $s;
	}

	private function trailingslashit(string $s): string {
		return (rtrim($s, '/\\' ) . '/');
	}

	/**
	 * Generates string containing all the mdoules.
	 * 
	 * @param  array  $modules List of modules to load.
	 * @return string String suitable to use in templates.
	 */
	private function getLoadModulesString(array $modules): string {
		$load_modules = '';
		foreach ($modules as $namespace_name) {
			$load_modules .= $this->env->render(
				'snippet-active-module.txt', 
				['namespace_name' => $namespace_name]
			);
		}

		return $load_modules;
	}

	/**
	 * Asks for basic info about the plugin.
	 *
	 * Every Wordpress plugin needs to have header in main file. Values that need to be in 
	 * this header are specified with this command.
	 * 
	 * @return void
	 * @command begin
	 */
	function begin(): void {
		if (file_exists(self::IPB_INFO_FILE)) {
			$this->say("File: " . self::IPB_INFO_FILE . " found. Exiting.");
			return;
		}

		$this->say("Let's set some parameters, so you can focus on writing amazing plugin.");

		$info = [
			'plugin_name' 			=> [
				'question'	=> "Plugin Name",
				'value'		=> "Intrigo Plugin Box"
			],

			'plugin_uri'			=> [
				"question"	=> "Plugin URI",
				'value'		=> "https://gitlab.com/Zag/intrigo-plugin-box"
			],
			'plugin_description'	=> [
				'question'		=> "Description",
				'value'			=> "Another way of building Wordpress plugins."
			],
			'plugin_version'		=> [
				'question'		=> "Version",
				'value'			=> "v0.0.0"
			],
			'plugin_author'			=> [
				'question'		=> "Author",
				'value'			=> "Matic Zagmajster"
			],
			'plugin_author_uri'		=> [
				'question'		=> "Author URI",
				'value'			=> "http://maticzagmajster.ddns.net/"
			]
		];

		foreach ($info as $prop => $sub) {
			$value = $this->ask($sub['question'] . " (Default: " . $sub['value'] . ") INPUT: ");
			if ($value !== NULL) {
				$info[$prop]['value'] = $value;
			}
		}

		$content = [];
		foreach ($info as $prop => $sub) {
			$content[$prop] = $sub['value'];
		}
		$content['load_modules'] = ['_all' => []];
		$json_data = json_encode($content, JSON_PRETTY_PRINT);
		file_put_contents(self::IPB_INFO_FILE, $json_data);

		// Generate main plugin file.
		$content['load_modules'] = '';
		$destination = realpath(self::MODULE_BASE_PATH) . '/' . $this->slugify($content['plugin_name']) . '.php';
		$status = $this->generateFile('index.php', $content, $destination);

		if ($status == 'FILE_CREATED') {
			$this->say("Main plugin file was created: " . $destination);
		}

	}

	/**
	 * Generates new module.
	 * 
	 * @param  string $namespace_name Name of new module.
	 * @return void
	 * @command: module:create
	 */
	function moduleCreate(string $namespace_name): void {
		$source_module_path = realpath(self::SOURCE_MODULE_PATH);
		$destination_module_path = realpath(self::MODULE_BASE_PATH) . '/' . $namespace_name;

		if (file_exists($destination_module_path)) {
			$this->say("It appears this module already exists. Exiting.");
			return;
		}

		$this->copyDir($source_module_path, $destination_module_path);

		// Some elements are required just in RefineIt module and others are using template.
		$remove_elements = [
			$this->config['structure']['config_folder']['name'] . '/' . $this->config['structure']['config_folder']['children']['structure_file']['name'],
			$this->config['structure']['support_folder']['name']
		];

		$files_from_templates = [
			$this->config['structure']['info_file']['name'],
			$this->config['structure']['plugin_shell_file']['name']
		];

		$remove_elements = array_merge($remove_elements, $files_from_templates);

		foreach ($remove_elements as $relative) {
			$path = $destination_module_path . '/' . $relative;
			$this->_exec('rm -rf ' . $path);
		}

		foreach ($files_from_templates as $file) {
			$destination = $destination_module_path . '/' . $file;
			file_put_contents($destination, $this->env->render($file, ['namespace_name' => $namespace_name]));
		}

		$this->say("New module: " . $namespace_name . " created!");
	}

	/**
	 * Creates new model inside given module.
	 * 
	 * @param  string $namespace_name Name of the module.
	 * @param  string $model_name     Name of the model.
	 * @return void
	 * @command model:create
	 */
	function modelCreate(string $namespace_name, string $model_name): void {
		$destination = realpath(self::MODULE_BASE_PATH) . '/' . $namespace_name . '/' . $this->config['structure']['models_folder']['name'] . '/' . $model_name . '.php';

		$status = $this->generateFile(
			'Model.php',
			['namespace_name' => $namespace_name, 'model_name' => $model_name],
			$destination
		);
		if ($status == 'FILE_ALREADY_EXISTS') {
			return;
		}

		$out = $namespace_name . '\\' . $this->config['structure']['models_folder']['name'] . '\\' . $model_name;
		$this->say("New model: " . $out . " was created.");
	}

	/**
	 * Creates new controller inside given module.
	 * 
	 * @param  string $namespace_name  Name of the module.
	 * @param  string $controller_name Name of the controller.
	 * @return void
	 * @command controller:create
	 */
	function controllerCreate(string $namespace_name, string $controller_name): void {
		$destination = realpath(self::MODULE_BASE_PATH) . '/' . $namespace_name . '/' . $this->config['structure']['controllers_folder']['name'] . '/' . $controller_name . '.php';
		
		$status = $this->generateFile('Controller.php', ['namespace_name' => $namespace_name, 'controller_name' => $controller_name], $destination);
		if ($status == 'FILE_ALREADY_EXISTS') {
			return;
		}

		$out = $namespace_name . '\\' . $this->config['structure']['controllers_folder']['name'] . '\\' . $controller_name;
		$this->say("New controller: " . $out . " was created.");
	}

	/**
	 * Creates plugin main file.
	 * 
	 * @return void
	 * @command plugin:create
	 */
	function pluginCreate(array $opts = ['target|t' => '']): void {
		// Load data.
		$content = json_decode(file_get_contents(self::IPB_INFO_FILE), true);

		if (!isset($content['load_modules'][$opts['target']])) {
			$this->say("Target: " . $opts['target'] . " does not exist. Exiting.");
			return;
		}

		$content['load_modules'] = $this->getLoadModulesString($content['load_modules'][$opts['target']]);

		// Regenerate main plugin file.
		$destination = realpath(self::MODULE_BASE_PATH) . '/' . $this->slugify($content['plugin_name']) . '.php';
		$this->generateFile('index.php', $content, $destination, true);

		$this->say("Main plugin file was created: " . $destination);
	}

	/**
	 * Release the plugin
	 *
	 * Use "install" option when you would like to install dependencies along side the plugin.
	 * Use this command without "install" switch if you would like to create dist "package" suitable for 
	 * end-user 
	 * 
	 * @param  string $destination Absolute path to new directory, plugin root folder should be.
	 * @param  array  $opts        Array of available options.
	 * @return void
	 */
	function pluginRelease(string $destination, array $opts = ['install|i' => false]): void {

		if (!file_exists($destination)) {
			$this->say("Destination: $destination does not exist.");
			return;
		}

		$content = json_decode(file_get_contents(self::IPB_INFO_FILE), true);

		// Lets make a "package" for distribution.
		$dir_name = $this->slugify($content['plugin_name']) . '_' . str_replace('.', '_', substr($content['plugin_version'], 1));

		if ($opts['install']) {
			$dir_name = $this->slugify($content['plugin_name']);
		}

		$path = $this->trailingslashit($destination) . $dir_name;
		$source_path = realpath('.');
		mkdir($path);

		// Copy source.
		$task = 'cp -r ' . $source_path . '/* ' . $path;
		$this->_exec($task);

		// Copy AUTHORS, LICENSE
		$this->_exec('cp ' . $source_path . '/../AUTHORS ' . $path);
		$this->_exec('cp ' . $source_path . '/../LICENSE ' . $path);

		// Delete some files (we will install production packages only).
		$this->_exec('rm -rf ' . $path . '/vendor');
		$this->_exec('rm -rf ' . $path . '/Support/.git');

		// Install dependencies on production. Please make sure 'composer' command is available on the server.
		if ($opts['install']) {
			$this->_exec('cd ' . $path . ' && composer install --no-dev');
		}
	}

}