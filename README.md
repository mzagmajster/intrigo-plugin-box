# Intrigo Plugin Box

Base for flexible and easy extensible Wordpress plugin.

## Getting Started

### Prerequisites

You need to have [Composer](https://getcomposer.org/) installed to be able to run this project and PHP version 7.

System needs the zip and nzip packages as well.

We also need Intl extension ( php7.2-intl )

### Installing

**This is meant to provide instructions on how to setup the enviornment for development of Intrigo Plugin Box so instructions below are not suitable for developing real plugin!**

Clone repository in directory that is not publicly accessible.

```
git clone https://gitlab.com/Zag/intrigo-plugin-box
```

Move into RefineIt module and clone support project as it is shown below.

```
git clone https://gitlab.com/Zag/refine-it-support.git Support
```

Move to plugin /src directory and install dependencies.

```
cd ../
composer install --dev
```

Define basic info about the plugin using Robo tool.

```
vendor/bin/robo begin
```

Create a symbolic link of a repository's src folder to real plugin dir

```
cd <wp-plugin-path>
ln -s <path-to-local-copy>/src intrigo-plugin-box
```

Plugin should now be visible on Wordpress plugin page. Use ```robo``` tool for other tasks.

## Running the tests

todo

## Deployment

1. Get entire plugin source to directory that is not accessible over the web.
1. Install all dependencies using ```composer```.
1. Create main plugin file if it does not exist in your source using command: ```vendor/bin/robo plugin:create -t <target>```.
1. Release plugin using command: ```vendor/bin/robo plugin:release <destination-path>```.

## Changelog

You can find log of changes inside ``CHANGELOG.md`` file.

## Documentation

Documetation for RefineIt Plugin can be found [here](https://maticzagmajster.ddns.net/docs/intrigo-plugin-box/).

## Built With

* [PHP](http://www.dropwizard.io/1.0.2/docs/) - Programming language
* [Composer](https://getcomposer.org/) - Dependency Management
* [Robo](https://robo.li/) - Command line tool used to speed up the process
* [Twig](https://twig.symfony.com/) - Templating engine

## Contributing

Please read ```CONTRIBUTING.md``` for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository. 

## Authors

Project has multiple authors, please see ```AUTHORS``` file for details.

## License

This project is licensed under the GNU v3.0 License - see the ```LICENSE``` file for details.

## Acknowledgments

* Big thanks goes to @theantichris for inspiration on this project.
